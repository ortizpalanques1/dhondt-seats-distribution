# dHondt seats distribution

Allocates seats according to a d'Hondt proportional distribution using r

# What it does
1. This function creates a *.txt table with the names of the political parties and their corresponding seats distributed according to the d'Hondt electoral system.
2. Using the features of the package [ggparliament](https://github.com/RobWHickman/ggparliament), this function creates a *.png file. It contains the graphical representation of seats distributed according to the d'Hondt electoral system.


# Requiered Libraries
1. dplyr
2. ggplot2
3. ggparliamentary

# Data
1. "party": A vector containing the names of the political parties.
2. "vote": A vector containing the number of votes for those political parties. Must be in the same order of the parties.
3. "colour": A vector containing the colour of the parties in hex code. 
4. "seat": The number of seats of the chamber.
5. "year": The year of the election (number).
6. "place": Country or region (quoted string).
7. "chamber": The name of the Chamber (quoted string)
8. "threshold": The electoral threshold (number; as proportion between 0 and 1, not as percentage)

# Using the function
1. Load the libraries
2. Create the vectors (Data 1 to 3). You can use a data.frame or three separated vectors
3. Fill the elements inside the parenthesis withe the name of the vectors or the data: dHondt(party,vote,colour,seat,year,place,chamber,threshold)
